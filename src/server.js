import express, { Router } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import routes from './routes';

const app = express();
const port = 3001;

const corsOption = {
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
};
app.use(cors(corsOption));
app.use(bodyParser.json());

app.get('/', (req, res) => res.send('Hello World!'));
app.use('/api', routes);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
