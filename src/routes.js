import { Router } from 'express';
import os from 'os';

const router = new Router();

router.get('/cpuLoadAverage', (req, res) => {
  const cpusLength = os.cpus().length;
  const cpuLoadAverage = os.loadavg()[0] / cpusLength;

  return res.status(200).json({ success: true, cpuLoadAverage });
});

export default router;
